package pages

import (
	"strconv"
)

func getQueryInt(q Query, key string, def int) int {
	val := q(key)
	if val == "" {
		return def
	}
	i, err := strconv.Atoi(val)
	if err != nil {
		return def
	}
	return i
}
