package pages_test

import (
	"errors"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/pages"
	"gitlab.com/thegalabs/go/pages/mocks"
)

func makeQuery(page int, count int) *mocks.Query {
	c := &mocks.Query{}
	c.On("Execute", "page").Return(strconv.Itoa(page))
	c.On("Execute", "count").Return(strconv.Itoa(count))
	return c
}

func TestMakeSuccess(t *testing.T) {
	q := makeQuery(2, 20)
	page, err := pages.Make(
		q.Execute,
		func(offset int, limit int) (interface{}, error) {
			return make([]interface{}, 10), nil
		},
		func() (int64, error) {
			time.Sleep(100 * time.Millisecond)
			return 100, nil
		},
	)

	assert.Nil(t, err)
	assert.Equal(t, 1, *page.Previous)
	assert.Equal(t, 3, page.Next)
	assert.Equal(t, int64(100), page.Count)
	assert.NotNil(t, page.Results)

	q.AssertExpectations(t)
}

func TestMakeCountError(t *testing.T) {
	expected := errors.New("Bro")
	q := makeQuery(2, 20)

	page, err := pages.Make(
		q.Execute,
		func(offset int, limit int) (interface{}, error) {
			return make([]interface{}, 10), nil
		},
		func() (int64, error) {
			time.Sleep(100 * time.Millisecond)
			return 0, expected
		},
	)

	assert.Nil(t, page)
	assert.Equal(t, expected, err)
	q.AssertExpectations(t)
}
