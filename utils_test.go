package pages

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/pages/mocks"
)

func TestQueryInt(t *testing.T) {
	generator := func(query string, expected int) func(*testing.T) {
		return func(t *testing.T) {
			q := &mocks.Query{}
			q.On("Execute", "p").Return(query)

			value := getQueryInt(q.Execute, "p", 10)
			assert.Equal(t, expected, value)
			q.AssertExpectations(t)
		}
	}

	t.Run("Found", generator("1", 1))
	t.Run("Invalid", generator("Hello", 10))
	t.Run("Missing", generator("", 10))
}
